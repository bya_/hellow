FROM node:8
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 9090
ADD start.sh start.sh
RUN chmod +x start.sh
CMD ./start.sh

